# civitas-landingpage

## Resumo

Repositório para prática de javaScript. Projeto CIVITAS marketplace. Treinamento Técnico EJCM.

## 📖 Conteúdo
- HTML
- DOM
- JavaScript
- Fetch API
- HTTP protocol


## ⚠️ Pré- requisitos 
- Node.js
- json-server
 

## 🎲 Rodando o projeto

- Clonar o projeto

```
git clone https://gitlab.com/EllenSouza/civitas-landingpage.git
```


- Abrir na pasta do projeto

```
cd civitas-landingpage
```

- Instalando as dependências
``` 
npm install
```

- Iniciando a API
```
json-server --watch db-json
```
Caso não funcione:

```
npx json-server --watch db-json
```

- Abrir no navegador 

    - Abrir o arquivo index.html 

<br> 

- Para fechar o json-server e parar de acessar a API.  
No terminal onde o json-server está rodando:
```
CTRL + C 
```
<br>

# 👩‍💻 Autora 

<a href="https://gitlab.com/EllenSouza">
    <kbd>
        <img src="https://gitlab.com/uploads/-/system/user/avatar/11121886/avatar.png?width=400" width="150">
        <br>
    </kbd>
    <span>  Ellen Almeida </span>
</a>
