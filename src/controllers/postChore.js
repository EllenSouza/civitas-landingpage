/*requisição assíncrona para criar uma nova chore */

const postChores = async (name, email,password, dtNasc) => {
    try {
        const response = await fetch ("http://localhost:3000/chores", {
            method: "POST",
            headers:{
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name: `${name}`,
                email: `${email}`,
                password: `${password}`,
                dtNasc: `${dtNasc}`,
            }),
        });

        const content = await response.json();

        return content;

    } catch (error) {
        console.log(error);
    }
};

export default postChores;