import postChore from "./postChore.js";


/*manipulando com DOM*/
const inputName = document.querySelector("#name"); 
const inputEmail = document.querySelector("#email");
const inputPassword = document.querySelector("#password");
const inputDtNasc = document.querySelector("#inputDT");
const createBTN = document.querySelector(".register-button-clients");

/* dispara evento para criar nova chore */

createBTN.addEventListener("click", async (event)=>{
    event.preventDefault(); //para evitar que recarregue a página 

    const name=inputName.value;
    const email=inputEmail.value;
    const password=inputPassword.value;
    const dtNasc=inputDtNasc.value;

    /*não precisa verificar se o campo está vazio  */
    await postChore(name,email,password,dtNasc);
    
});